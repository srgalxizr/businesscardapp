// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBchxW3C3Y26LckMZkA1Aj_MsK5PejKg80",
    authDomain: "businesscardapp-2d0d7.firebaseapp.com",
    databaseURL: "https://businesscardapp-2d0d7.firebaseio.com",
    projectId: "businesscardapp-2d0d7",
    storageBucket: "businesscardapp-2d0d7.appspot.com",
    messagingSenderId: "279097934266",
    appId: "1:279097934266:web:b1c95c9ed3de40ff015d83",
    measurementId: "G-2S54FMJ1PB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
