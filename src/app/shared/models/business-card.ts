import { IImages } from "./images";

export interface IBusinessCard {
  id?: string;
  images?: IImages;
}
