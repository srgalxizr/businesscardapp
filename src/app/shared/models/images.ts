export interface IImages {
  urls: Array<string>;
  names: Array<string>;
}
