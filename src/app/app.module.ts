import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { APP_INITIALIZER } from "@angular/core";

import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { environment } from "../environments/environment";

import { AppRoutingModule } from "./app-routing.module";
import { MaterialPrimeNGModule } from "./modules/material-prime-ng/material-prime-ng.module";

import { AppComponent } from "./app.component";
import { HomeComponent } from "./components/Home/home.component";
import { LoginRegisterComponent } from "./components/Auth/login-register.component";
import { BusinessCardComponent } from "./components/BusinessCard/business-card.component";
import { HeaderComponent } from "./components/header/header.component";
import { PreLoadService } from "./services/preLoad/pre-load.service";

//Pre Load service Factory function
export function preLoadFactory(provider: PreLoadService) {
  return () => provider.getBrowserName();
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginRegisterComponent,
    BusinessCardComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    MaterialPrimeNGModule
  ],
  providers: [
    PreLoadService,
    {
      provide: APP_INITIALIZER,
      useFactory: preLoadFactory,
      deps: [PreLoadService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
