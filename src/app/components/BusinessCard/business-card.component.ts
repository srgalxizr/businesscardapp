import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { FirestoreDBService } from "src/app/services/DB/firestore-db.service";
import { AuthService } from "src/app/services/Auth/auth.service";
import { StorageService } from "../../services/Storage/storage.service";
import { Subscription } from "rxjs";
import { IBusinessCard } from "../../shared/models/business-card";
import { IImages } from "../../shared/models/images";

@Component({
  selector: "app-business-card",
  templateUrl: "./business-card.component.html",
  styleUrls: ["./business-card.component.css"]
})
export class BusinessCardComponent implements OnInit, OnDestroy {
  dbSubscription: Subscription = null;

  edit = false;

  i = 0;
  files: FileList;
  currentSrc: string = "";
  currentName: string = "";

  businessCardForm: FormGroup;
  uid: string;
  cardData: firebase.firestore.DocumentData = null;

  constructor(
    public readonly _db: FirestoreDBService,
    public readonly _auth: AuthService,
    public readonly _storage: StorageService
  ) {
    this.cardData = JSON.parse(localStorage.getItem("data"));
    if (!!this.cardData && !!this.cardData.images) {
      this.currentSrc = this.cardData.images.urls[this.i];
      this.currentName = this.cardData.images.names[this.i];
      this.next();
      this.prev();
    }
  }

  ngOnInit() {
    this._buildForm();

    if (this.cardData) {
      this._buildForm(this.cardData);
    }

    this.uid = this._auth._currentLoggedInUser.user.uid;
    this._storage.uid = this.uid;

    this._auth.qrcode += window.location.href + "/" + this.uid;

    if (!this.cardData) {
      this.dbSubscription = this._db.getBusinessCard(this.uid).subscribe(
        doc => {
          this.cardData = doc.data() as IBusinessCard;

          // // we must set the 'images' key on the data object for the upload method will not fail as it is looking for it
          // if (Object.keys(this.cardData)["images"] === undefined) {
          //   this.cardData["images"] = {
          //     urls: [],
          //     names: []
          //   };
          // }

          localStorage.setItem("data", JSON.stringify(doc.data()));

          this._buildForm(this.cardData);
        },
        err => {
          console.log("error", err);
        }
        // end => {
        //   console.log("the end");
        // }
      );
    }
  } //ng on init

  ngOnDestroy() {
    if (this.dbSubscription) {
      console.log(this.dbSubscription);
      this.dbSubscription.unsubscribe();
    }
  }

  async onSubmit() {
    if (this.businessCardForm.valid && !!this.uid) {
      if (this.checkMadeChanges(this.businessCardForm)) {
        await this._db.addBusinessCard(this.uid, this.businessCardForm.value);

        if (this.files && this.files.length > 0) {
          await this._storage.uploadFile(this.files, this.cardData);
        }
      } //if changes made
    } // if authenticated and form is valid
  } //on submit

  async _buildForm(data?: firebase.firestore.DocumentData) {
    if (!data) {
      this.businessCardForm = new FormGroup({
        email: new FormControl(null, [Validators.required, Validators.email]),
        phone: new FormControl(null, [Validators.required]),
        fax: new FormControl(null /*, [Validators.required]*/),
        website: new FormControl(null /*, [Validators.required]*/),
        youtube: new FormControl(null /*, [Validators.required]*/),
        facebook: new FormControl(null /*, [Validators.required]*/),
        twitter: new FormControl(null /*, [Validators.required]*/),
        instagram: new FormControl(null /*, [Validators.required]*/),
        images: new FormControl(null)
      });
    } else {
      this.businessCardForm.get("email").setValue(data.email);
      this.businessCardForm.get("facebook").setValue(data.facebook);
      this.businessCardForm.get("instagram").setValue(data.instagram);
      this.businessCardForm.get("twitter").setValue(data.twitter);
      this.businessCardForm.get("youtube").setValue(data.youtube);
      this.businessCardForm.get("website").setValue(data.website);
      this.businessCardForm.get("fax").setValue(data.fax);
      this.businessCardForm.get("phone").setValue(data.phone);
      this.businessCardForm.get("images").setValue(null);
    }
  }

  next() {
    this.i++;

    if (this.i >= this.cardData.images.urls.length) {
      this.i = this.cardData.images.urls.length - 1;
    }

    this.currentSrc = this.cardData.images.urls[this.i];
    this.currentName = this.cardData.images.names[this.i];
  }

  prev() {
    this.i--;

    if (this.i < 0) {
      this.i = 0;
    }

    this.currentSrc = this.cardData.images.urls[this.i];
    this.currentName = this.cardData.images.names[this.i];
  }

  async onChangeFiles(event) {
    this.files = event.target.files;
    this.i = 0;

    //manually set form status
    if (this.files.length > 5) {
      this.businessCardForm.setErrors({
        incorrect: true,
        message: "You have selected too many images"
      });
      // console.log(this.businessCardForm.errors);
    } else if (
      JSON.parse(localStorage.getItem("data"))["images"] !== undefined &&
      JSON.parse(localStorage.getItem("data"))["images"]["names"] !==
        undefined &&
      JSON.parse(localStorage.getItem("data"))["images"]["names"].length +
        this.files.length >
        5
    ) {
      this.businessCardForm.setErrors({
        incorrect: true,
        message:
          "Your images count exceeds the legal quota, please delete some of your 'saved' imgages before selecting new"
      });
      // console.log(this.businessCardForm.errors);
    } else {
      this.businessCardForm.setErrors(null);
    }
  }

  onDeleteImageStorage(event, name: string) {
    let position = this.cardData.images.names.indexOf(name);
    this.cardData.images.names.splice(position, 1);
    this.cardData.images.urls.splice(position, 1);

    this._storage.deleteFile(null, name);
    this.next();

    if (this.cardData.images.names.length == 0) {
      delete this.cardData.images;
      localStorage.setItem("data", JSON.stringify(this.cardData));
    }
  }

  checkMadeChanges(newData) {
    let oldData = localStorage.getItem("data");
    if (!!oldData) {
      oldData = JSON.parse(oldData);

      Object.keys(oldData).some(key => {
        if (newData.value[key] !== oldData[key]) {
          //if match is FALSE then there was a change and need to update
          console.log("match is false", newData.value[key], oldData[key]);
          return true;
        }

        return false;
      });
    } //if

    // always return true if there is no cache or previousely known data to create new
    return true;
  }
} //class
