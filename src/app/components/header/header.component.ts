import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { AuthService } from "../../services/Auth/auth.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  menuOpen = false;
  user: firebase.User;
  constructor(public readonly _auth: AuthService) {}

  ngOnInit() {
    this._auth.getAuthState().subscribe(u => {
      this.user = u;
    });
  }

  onLogout() {
    this._auth.logoutUser();
  }

  menu() {
    this.menuOpen = !this.menuOpen;
  }
}
