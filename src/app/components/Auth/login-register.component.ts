import { Component, OnInit, AfterViewInit } from "@angular/core";

import { FormGroup, FormControl, Validators } from "@angular/forms";
import { FirestoreDBService } from "src/app/services/DB/firestore-db.service";
import { AuthService } from "src/app/services/Auth/auth.service";
import { RaiseToastService } from "../../modules/material-prime-ng/services/raise-toast.service";

@Component({
  selector: "app-login-register",
  templateUrl: "./login-register.component.html",
  styleUrls: ["./login-register.component.css"]
})
export class LoginRegisterComponent implements OnInit, AfterViewInit {
  authForm: FormGroup;
  authMode: boolean = false; //authmode true > register | authmode false > login

  constructor(
    public readonly _db: FirestoreDBService,
    public readonly _auth: AuthService,
    private readonly _rt: RaiseToastService
  ) {}

  ngOnInit() {
    this.authForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  } //ng on init

  ngAfterViewInit() {
    this._rt.raiseToast({
      summary: "Please log in.",
      detail: "Authentication page",
      severity: "success",
      closable: true,
      life: 5000
    });
  }

  async onSubmit() {
    let res;
    if (this.authForm.valid) {
      if (this.authMode) {
        res = await this._auth.createUser(this.authForm.value);
        if (res) {
          this._rt.raiseToast({
            summary: "Registration Successful",
            detail: "Please log in.",
            severity: "success",
            closable: true,
            life: 5000
          });
        } else {
          this._rt.raiseToast({
            summary: "Registration Failure",
            detail: "Please try again",
            severity: "error",
            closable: true,
            life: 3000
          });
        }
      } else {
        res = await this._auth.loginUser(this.authForm.value);
        if (!res) {
          this._rt.raiseToast({
            summary: "Login Failure",
            detail: "Please try again and provide correct credentials.",
            severity: "error",
            closable: true,
            life: 3000
          });
        }
      }
    }

    if (!res) {
      this._rt.raiseToast({
        summary: "Please log in.",
        detail: "Authentication page",
        severity: "success",
        closable: true,
        life: 5000
      });
    }
  } //on submit

  changeAuthMode() {
    this.authMode = !this.authMode;
    console.log(this.authMode);
  }
} //class
