import { NgModule, Component } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginRegisterComponent } from "./components/Auth/login-register.component";
import { HomeComponent } from "./components/Home/home.component";
import { BusinessCardComponent } from "./components/BusinessCard/business-card.component";
import { AuthGuard } from "./shared/guards/auth.guard";

const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
    children: [
      { path: "", redirectTo: "auth", pathMatch: "full" },
      { path: "auth", component: LoginRegisterComponent },
      {
        path: "business-card",
        component: BusinessCardComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  // {path: "testprod": Component: testprod}
  { path: "**", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
