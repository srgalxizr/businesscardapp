import { TestBed } from '@angular/core/testing';

import { RaiseToastService } from './raise-toast.service';

describe('RaiseToastService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RaiseToastService = TestBed.get(RaiseToastService);
    expect(service).toBeTruthy();
  });
});
