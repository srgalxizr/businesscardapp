import { Injectable, ApplicationRef } from "@angular/core";
import { MessageService } from "primeng/api";

@Injectable()
export class RaiseToastService {
  constructor(
    readonly _messageService: MessageService,
    readonly _ar: ApplicationRef
  ) {}

  raiseToast(t: IToast) {
    this._messageService.add({
      ...t
    });
    this._ar.tick();
  }
} // class

type severity = "success" | "info" | "warn" | "error";

interface IToast {
  closable: boolean;
  severity: severity;
  summary: string;
  detail: string;
  life?: number;
}
