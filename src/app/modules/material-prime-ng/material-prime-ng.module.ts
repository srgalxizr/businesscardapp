import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { ToastModule } from "primeng/toast";
import { MessageService } from "primeng/api";
import { MatButtonModule } from "@angular/material";

import { RaiseToastService } from "../material-prime-ng/services/raise-toast.service";

const material_primeng_modules = [ToastModule, MatButtonModule];

@NgModule({
  imports: [material_primeng_modules],
  exports: [material_primeng_modules],
  providers: [MessageService, RaiseToastService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MaterialPrimeNGModule {}
