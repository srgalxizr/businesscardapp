import { Component, OnInit } from "@angular/core";
import { AuthService } from "./services/Auth/auth.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "BusinessCardApp";

  constructor(public readonly _auth: AuthService) {}
  ngOnInit() {
    this._auth.autoLogin();
  }
} //class
