import { Injectable } from "@angular/core";
import { of } from "rxjs";

@Injectable()
export class PreLoadService {
  constructor() {}

  public getBrowserName() {
    const agent = window.navigator.userAgent.toLowerCase();
    let name: string = "Your Browser is: ";

    switch (true) {
      case agent.indexOf("edge") > -1:
        name += "edge";
        break;
      case agent.indexOf("opr") > -1 && !!(<any>window).opr:
        name += "opera";
        break;
      case agent.indexOf("chrome") > -1 && !!(<any>window).chrome:
        name += "chrome";
        break;
      case agent.indexOf("trident") > -1:
        name += "ie";
        break;
      case agent.indexOf("firefox") > -1:
        name += "firefox";
        break;
      case agent.indexOf("safari") > -1:
        name += "safari";
        break;
      default:
        name += "other";
        break;
    } // switch

    return of(name)
      .toPromise()
      .then(data => {
        //      console.log("navigator", this.getBrowserName());
        console.log("navigator", data);
      });
  } // private method get browser name
}
