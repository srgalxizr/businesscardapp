import { TestBed } from '@angular/core/testing';

import { PreLoadService } from './pre-load.service';

describe('PreLoadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PreLoadService = TestBed.get(PreLoadService);
    expect(service).toBeTruthy();
  });
});
