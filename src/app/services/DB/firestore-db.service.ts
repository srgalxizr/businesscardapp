import { Injectable } from "@angular/core";
import { Collections } from "../../shared/enums/collections.enum";
import { Observable } from "rxjs";
import { map, take, exhaustMap } from "rxjs/operators";
import {
  AngularFirestoreCollection,
  AngularFirestore
} from "@angular/fire/firestore";
import { IBusinessCard } from "../../shared/models/business-card";

@Injectable({
  providedIn: "root"
})
export class FirestoreDBService {
  _itemCollection: AngularFirestoreCollection<IBusinessCard>;
  _items: Observable<Array<any>>;

  constructor(public readonly _angularFirestore: AngularFirestore) {
    this._itemCollection = this._angularFirestore.collection(
      Collections.BusinessCards
      //ref => ref.orderBy("title", "asc")
    );

    this._items = this._itemCollection.snapshotChanges().pipe(
      map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as IBusinessCard;
          data["id"] = a.payload.doc.id;

          return data;
        });
      })
    );
  }

  //get All Business Cards
  async getBusinessCards() {
    return await this._items;
  }

  //get Business Card
  getBusinessCard(id: string) {
    console.log("get bc db service");
    return this._itemCollection.doc(id).get();
  }

  //delete Business Cards
  async deleteBusinessCard(id: string) {
    await this._itemCollection.doc(id).delete();
  }

  //new Business Cards
  async addBusinessCard(id: string, newCard: IBusinessCard) {
    localStorage.setItem("data", JSON.stringify(newCard));
    await this._itemCollection.doc(id).set(newCard);
  }

  //update Business Cards
  async updateBusinessCard(id: string, newCard: IBusinessCard) {
    await this._itemCollection.doc(id).update(newCard);
  }
} //class
