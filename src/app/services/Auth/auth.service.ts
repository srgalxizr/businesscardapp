import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
import { BehaviorSubject, Observable, throwError } from "rxjs";
import { map } from "rxjs/operators";
import { Router, ActivatedRoute } from "@angular/router";
import { IUser } from "../../shared/models/user";
import { Collections } from "../../shared/enums/collections.enum";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  _currentLoggedInUser: firebase.auth.UserCredential = null;
  isAuthenticated = new BehaviorSubject<boolean>(null);
  qrcode = "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=";

  constructor(
    public readonly _angularFirebaseAuth: AngularFireAuth,
    public readonly _angularFirestore: AngularFirestore,
    public readonly _router: Router,
    public readonly _activatedRoute: ActivatedRoute
  ) {}

  async createUser(user: IUser) {
    let newuser: firebase.auth.UserCredential;
    try {
      newuser = await this._angularFirebaseAuth.auth.createUserWithEmailAndPassword(
        user.email,
        user.password
      );

      console.log(user);
      console.log(newuser);

      await this._angularFirebaseAuth.auth.onAuthStateChanged(user => {
        !!user.sendEmailVerification();

        console.log("send email verification", user);
      });

      await this._createUserData(user, newuser);

      this._router.navigate([""], {
        relativeTo: this._activatedRoute
      });
      console.log("redirect doesnt work... need to set up routing");
    } catch (e) {
      console.log("failed to create new user");
      console.log(e);
    }
  }

  async _createUserData(user: IUser, userData: firebase.auth.UserCredential) {
    try {
      await this._angularFirestore
        .collection(Collections.Users)
        .doc(userData.user.uid)
        .set({ ...user });

      console.log("create user data");
    } catch (e) {
      console.log("create user data failed", e);
    }
  }

  async loginUser(user: IUser) {
    try {
      this._currentLoggedInUser = await this._angularFirebaseAuth.auth.signInWithEmailAndPassword(
        user.email,
        user.password
      );

      localStorage.setItem(
        "currentUser",
        JSON.stringify(this._currentLoggedInUser)
      );

      this.isAuthenticated.next(true);
      this._router.navigate(["/business-card"]);
    } catch (e) {
      console.log("result null after login attempt");
      console.log(e);
      return false;
    }
  }

  async logoutUser() {
    console.log("logout user");
    try {
      localStorage.removeItem("currentUser"); //delete only user data and leave the user details for caching purposes
      this._currentLoggedInUser = null;
      this.isAuthenticated.next(null);
      await this._angularFirebaseAuth.auth.signOut();
      this._router.navigate(["/"]);
    } catch (e) {
      console.log("logout failed");
      console.log(e);
    }
  }

  getAuthState(): Observable<firebase.User> {
    return this._angularFirebaseAuth.authState;
  }

  autoLogin() {
    this._currentLoggedInUser = JSON.parse(localStorage.getItem("currentUser"));
    if (!!this._currentLoggedInUser) {
      this.isAuthenticated.next(true);
    }
  }
} // class
