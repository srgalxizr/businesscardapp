import { Injectable } from "@angular/core";
import {
  AngularFireStorage,
  AngularFireUploadTask,
  AngularFireStorageReference
} from "@angular/fire/storage";
import { map, finalize, tap, take } from "rxjs/operators";
import { Observable } from "rxjs";
import { FirestoreDBService } from "../DB/firestore-db.service";
import { IBusinessCard } from "../../shared/models/business-card";
import { IImages } from "../../shared/models/images";

@Injectable({
  providedIn: "root"
})
export class StorageService {
  // task: AngularFireUploadTask;
  // ref: AngularFireStorageReference;
  // uploadProgress: Observable<number>;
  // downloadURL: Observable<string>;
  // uploadState: Observable<string>;

  task: Array<AngularFireUploadTask>;
  ref: Array<AngularFireStorageReference>;
  uploadProgress: Array<Observable<number>>;
  downloadURL: Array<Observable<string>>;
  uploadState: Array<Observable<string>>;

  _uid: string;
  _maxImages = 5;
  _ifiles;

  constructor(
    public readonly _storage: AngularFireStorage,
    public readonly _db: FirestoreDBService
  ) {}

  set uid(uid: string) {
    this._uid = uid;
  }

  async uploadFile(files, bcData: IBusinessCard) {
    let images: IImages;

    this.downloadURL = new Array();
    this.task = new Array();
    this.ref = new Array();
    this.uploadProgress = new Array();
    this.uploadState = new Array();

    //assign images object or create new
    if (bcData.images) {
      console.log("copy images");
      images = bcData.images;
    } else {
      images = <IImages>Object();
      images.urls = new Array();
      images.names = new Array();
      console.log("create images");
    }

    if (!!images && images.urls.length + files.length > this._maxImages) {
      let url, name;

      for (let i = 0; i < files.length; i++) {
        url = images.urls.shift();
        name = images.names.shift();
        this.deleteFile(null, name);
      }
    }

    for (let i = 0; i < files.length && i < this._maxImages; i++) {
      let bucketPath = `${this._uid}\/${files[i].name}`;

      this.ref.push(this._storage.ref(bucketPath));

      this.downloadURL.push(this.ref[i].getDownloadURL());

      let task = this._storage.upload(bucketPath, files[i]);
      this.task.push(task);
      console.log("uploading " + files[i].name);
      this.uploadProgress.push(task.percentageChanges());

      this.uploadState.push(
        task.snapshotChanges().pipe(
          map(s => {
            return s.state;
          })
        )
      );

      images.names.push(files[i].name);

      task
        .snapshotChanges()
        .pipe(
          tap(console.log),
          finalize(async () => {
            let downloadURL: string = await this.ref[i]
              .getDownloadURL()
              .toPromise();
            images.urls.push(downloadURL);

            //here need to push data to firestore db as well
            bcData["images"] = images;
            this._db.updateBusinessCard(this._uid, bcData);

            //if cached data exists then update with new values
            if (localStorage.getItem("data")) {
              localStorage.setItem("data", JSON.stringify(bcData));
            }
          })
        )
        .subscribe();
    } //for
  }

  async deleteFile(event, name: string) {
    console.log(`delete ${name}`);
    this._storage.ref(`${this._uid}\/${name}`).delete();

    if (event) {
      //this is used only to update the ui
      event.target.parentElement.parentElement.style.display = "none";
    }

    let bc = await this._db.getBusinessCard(this._uid).toPromise();
    let bcData = bc.data();

    let i = bcData.images.names.indexOf(name);
    bcData.images.names.splice(i, 1);
    bcData.images.urls.splice(i, 1);

    this._db.updateBusinessCard(this._uid, bcData);
  }

  async downloadFile() {
    console.log("need to implement");
  }
} //class
